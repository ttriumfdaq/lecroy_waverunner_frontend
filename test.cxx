
#include <iostream>

#include "TLecroyWaverunner.h"

/// Class for decoding the current template of the waveform data for Lecroy Waverunner.
/// See lecroy_template.txt for details...
class LecroyScopeData {


public:

  LecroyScopeData(std::string response){

    // There is some 21bit of nonsense at the start of the data packet... let's get rid of them.
    fString = response.substr(21);

    std::cout << response.substr(0,30) << std::endl;

    //std::cout << response.substr(response.length()-10,10) << std::endl;
    //std::cout << "Total length: " << response.length() << std::endl;

    // Check the total length... better be correct.
    int reported_length = atoi(response.substr(12,9).c_str());

    if(reported_length +1 != fString.length() && reported_length  != fString.length() ){
      std::cerr << "LecroyScopeData:: The reported length of data packet (" << reported_length 
		<< ") does not match actual length (" <<fString.length()  << std::endl;
	exit(0);
    }


    // WaveDesc part:

    bool verbose = true;
    if(verbose){
      //std::cout << "First 16 : " << GetString(0)
      //	<< std::endl;
      //      std::cout << "Second 16 : " << GetString(16) << std::endl;
      
	    //std::cout << "COMM_TYPE: " << GetData8()[32] << " " << std::endl;
      
      std::cout << "length WAVEDESC: "      << GetWaveDescLength() << std::endl;
      std::cout << "waveform data length: " << GetWaveArrayLength() << std::endl;
      //std::cout << "waveform data samples: " << GetWaveArraySamples() << std::endl;
      
      
      //std::cout << "Instrument name : " << GetInstrumentName() << std::endl;

     
      //std::cout << "Vertical gain "  << GetVerticalGain()   << std::endl;
      //std::cout << "Vertical offset "  << GetVerticalOffset()   << std::endl;
      //std::cout << "Horizontal Intervale "  << GetHorizontalInterval()   << std::endl;
      //std::cout << "Record type "  << GetRecordType()   << std::endl;
      //std::cout << "Coupling "  << GetCoupling() << std::endl;
      //std::cout << "Get comm order "  << GetCommOrder() << std::endl;

      std::cout << "Channel: " << GetChannel() << std::endl;

    }

    // Actual waveform information...
    
    
    if(0)
      for(int i =0; i < 20; i++){
      std::cout << i << " 0x" << std::hex << (int) GetData8(GetWaveDescLength())[i] << std::dec << " " << (int)GetData8(GetWaveDescLength())[i]
		<< " " << GetVoltageValue(0,i) << std::endl;
    }

  }


  ~LecroyScopeData(){

  }


private:

  std::string  fString;
  int fStringLength;
  
  const uint8_t*  GetData8(int byteoffset=0) const { return reinterpret_cast<const uint8_t*>(fString.c_str() + byteoffset); }
  const uint16_t* GetData16(int byteoffset=0) const { return reinterpret_cast<const uint16_t*>(fString.c_str() + byteoffset); }

  const uint32_t* GetData32(int byteoffset=0) const { return reinterpret_cast<const uint32_t*>(fString.c_str() + byteoffset); }

  float GetFloat(int byteoffset=0) const { return *reinterpret_cast<const float*>(fString.c_str() + byteoffset); }

  std::string GetString(int byteoffset=0) const { return fString.substr(byteoffset,16);}
  
public:
  
  // length of wave description block, in bytes
  int GetWaveDescLength(){ return GetData32(36)[0];};

  // length of waveform array (in bytes)
  int GetWaveArrayLength(){ return GetData32(60)[0];};

  // length of waveform samples.
  int GetWaveArraySamples(){ return GetData32(116)[0];};

  std::string GetInstrumentName(){ return GetString(76);};

  float GetVerticalGain(){  return GetFloat(156);  };

  float GetVerticalOffset(){  return GetFloat(160);  };

  // Get the sampling interval in time;
  float GetHorizontalInterval(){ return GetFloat(176);  };
  
  int GetRecordType(){  return GetData32(316)[0];  };

  int GetCoupling(){return GetData32(326)[0];};

  short GetCommOrder(){return GetData16(34)[0];};

  short GetChannel(){return GetData16(344)[0];};

  // Get voltage value for channel ch and sample i
  float GetVoltageValue(int ch, int i){

    if(i < 0 || i > 11111111) return -9999.0;

    int value = GetData8(GetWaveDescLength())[i];
    if(value >= 0x80) value = -256 + value ;

    int value2 = -99999;
    if(GetData8(GetWaveDescLength())[i] < 0x80)
      value2 = GetData8(GetWaveDescLength())[i];
    else
      value2 = - (int)((GetData8(GetWaveDescLength())[i] + 0x80) & 0xff);

    std::cout << "value " << (int)GetData8(GetWaveDescLength())[i] << " " << value << " " << value2 << std::endl;
    return (float)value *GetVerticalGain() - GetVerticalOffset();
  }

};



// Little test program that sets and reads the analog 
// output and input on Galil 47120. 
int main(int argc, const char * argv[]){

  TLecroyWaverunner *wave = new TLecroyWaverunner("142.90.103.48");

  // Send a generic identification request, to ensure that connection works. 

  for(int i = 0; i < 1; i++){
    std::string response = wave->SendRequestGetStringResponse("TIME_DIV?");
  }
  //  std::cout << "time division: "  << response << std::endl;
 
  std::string response = wave->SendRequestGetStringResponse("PAVA?");

  std::cout << "time division: "  << response << std::endl;


  std::string response2 = wave->SendRequestGetStringResponse("C1:INSPECT? \"WAVEDESC\"");

  std::cout << "waveform description: "  << response2 << std::endl;
 
  std::string response3 = wave->SendRequestGetStringResponse("waveform_setup?");

  //std::cout << "waveform setup: "  << response3 << std::endl;
  wave->SendCommand("TRMD SINGLE");
  usleep(100000);

  for(int i =0 ; i < 1; i++){
    

    //usleep(100000);
    std::cout << "\n_____________________________________________________________\n";
    
    
    wave->SendCommand("TRMD SINGLE");
    
    
    // Wait till trigger is ready...
    int j;
    for(j = 0; j < 100000; j++){
      std::string inr = wave->SendRequestGetStringResponse("INR?");
      int register_value = strtol(inr.substr(4,4).c_str(),NULL,0);
      if(register_value & 0x2000) break;

      usleep(1);
    }
    std::cout << "Found trigger after " << j << " tries." << std::endl;



    std::cout << "Waveform " << i << std::endl;
    std::string response4 = wave->GetWaveform("c1:waveform? all");
    LecroyScopeData decode(response4);
    std::cout << "Request two! " << std::endl;
    std::string response5 = wave->GetWaveform("c2:waveform? all");
    LecroyScopeData decode2(response5);
    std::cout << "Size of data: " << response4.length() << std::endl;
  }

  //   std::string response5 = wave->GetWaveform("waveform? all");
  //LecroyScopeData decode5(response5);
  // std::cout << "Size of data: " << response4.length() << std::endl;

  wave->SendCommand("TRMD AUTO");
  
 

  return 0;



}


