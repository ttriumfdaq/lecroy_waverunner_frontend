#ifndef TLecroyWaverunner_H
#define TLecroyWaverunner_H

#include "KOsocket.h"
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "VICPClient.h"
// A class for talking to Lecroy Waverunner 
// Tested with Waverunner 610ZI
// Uses the VICPClient to construct the correctly formatted data packets for
// talking to scope; VICPClient in turns uses KOSocket for TCP/IP connection.
class TLecroyWaverunner {

  

 public:

  // Create the TLecroyWaverunner and connect to the device.
  TLecroyWaverunner(std::string ip);


  ~TLecroyWaverunner(){delete fDataSocket;};

  bool SendCommand(std::string commandString);
  std::string SendRequestGetStringResponse(std::string requestString);

  std::string GetWaveform(std::string requestString);

  CVICPClient *fCVICPClient;
  
 private:
  
  KOsocket *fDataSocket;


};

#endif
