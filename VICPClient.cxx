#include "VICPClient.h"
#include <arpa/inet.h>
#include <sys/time.h>



CVICPClient::CVICPClient()
{
  // no connection (yet)
  m_socketFd = -1;
  m_readState = NetWaitingForHeader;
  m_connectedToScope = false;
  m_remoteMode = false;
  m_localLockout = false;
  m_iberr = m_ibsta = m_ibcntl = 0;
  m_bErrorFlag = false;
  m_nextSequenceNumber = 1;
  m_lastSequenceNumber = 1;
  m_bFlushUnreadResponses = true;
  m_bVICPVersion1aSupported = false;
  
  // TODO: determine the max. block size
  m_maxBlockSize = 512;		// TODO: safe value for now
  
  m_currentTimeout = 1;
}

bool CVICPClient::connectToDevice(std::string ip){
  
  // Open connection; port 1861 is magic port number for Lecroy VICP 
  fDataSocket = new KOsocket(ip.c_str(),1861);
  fDataSocket->setSoTimeout(m_currentTimeout); 
  
  m_connectedToScope = true;
  return(true);
}


bool CVICPClient::sendSmallDataToDevice(const char *message, int bytesToSend, bool eoiTermination, bool deviceClear, bool serialPoll){


  static unsigned char smallDataBuffer[SMALL_DATA_BUFSIZE + IO_NET_HEADER_SIZE + 2];
  int bytesSent;
  int bytesToSendWithHeader = bytesToSend + IO_NET_HEADER_SIZE;
  
  memcpy(&smallDataBuffer[IO_NET_HEADER_SIZE], message, bytesToSend);


  smallDataBuffer[0] = OPERATION_DATA;  
  smallDataBuffer[0] |= OPERATION_EOI;
  smallDataBuffer[1] = HEADER_VERSION1;

  static int sequence_number = 0;
  sequence_number++;

  smallDataBuffer[2] = GetNextSequenceNumber(smallDataBuffer[0]);	// = sequence_number;	 
  smallDataBuffer[3] = 0x00;											// unused
  *((unsigned int *) &smallDataBuffer[4]) = htonl(bytesToSend);		// message size

  

  //  std::cout << "write  " << bytesToSendWithHeader << std::endl;

  //f//or(int i = 0; i < bytesToSendWithHeader; i++)
      //std::cout << i  << ": " << smallDataBuffer[i] << std::endl;;
  //std::cout << std::endl;
  

  fDataSocket->write((char *) smallDataBuffer,bytesToSendWithHeader);
  //std::cout << "wrote " << std::endl;
 


  return true;

}
#include <chrono>

bool CVICPClient::stripVICPHeaders(std::string tmpString, std::string &stringResponse, bool &lastPacketHasEOI){

  // The temporary array will be filled with a series of different VICP packet.  Let's go through them, checking
  // that all the packets belong to the same sequence.
  int remaining_string_length = tmpString.length();
  int strpointer = 0;
  bool finished = false;
  int npackets_found = 0;

  lastPacketHasEOI = false;
  while (!finished){
    
    // The first VICP header should be right at start.
    unsigned int *asint = (unsigned int*) (tmpString.substr(strpointer,8).c_str());
    unsigned int packet_length = ntohl(asint[1]);
    unsigned int sequence_number = ((asint[0] & 0xff0000) >> 16);
    unsigned int shortstart = asint[0];


    npackets_found++;
    if(sequence_number != (unsigned int) GetLastSequenceNumber()){
      std::cerr << "CVICPClient::getStringResponse : sequence numbers don't match ("
		<< sequence_number << " != " << (unsigned int) GetLastSequenceNumber() 
		<< ") ; data corruption likely." << npackets_found << " : " << tmpString 
		<< " : " << tmpString.length() << std::endl;
      exit(0);
    }
    
    

    if(remaining_string_length <= packet_length + 8){
      finished = true;
      if(remaining_string_length != packet_length + 8){
	std::cerr << "CVICPClient::getStringResponse : got out of sequence " << remaining_string_length << " " <<  packet_length + 8 << " found EOI="
		  << (shortstart & 1) << std::endl;
	stringResponse = std::string("COM-FAILURE");
	//exit(0);
	return false;
      }
    }

    if(0)std::cout << "VICP header : " << std::hex << shortstart << std::dec <<" packet length " 
    	      <<packet_length 
	      << " " << remaining_string_length << " " <<  packet_length 
	      << " " << finished << std::endl;
    
    stringResponse.append(tmpString,strpointer+8,packet_length);
    strpointer += packet_length + 8;
    remaining_string_length -=  packet_length + 8;

    if(finished){    
      if(shortstart & 1)
	lastPacketHasEOI = true;
    }
  }

  return true;

}

bool CVICPClient::getStringResponse(std::string &stringResponse){

    using namespace std::chrono;
 
  // First we read all the available data into a temporary array.
  stringResponse = std::string();

  bool lastPacketHasEOI = false;;
  
  // Keep requesting TCP packets until we can reconstruct a VICP packet
  // that has the EOI bit set.
  // Break after 100 cycles;
  int ncycles = 0;
  while(!lastPacketHasEOI && ncycles < 100) {
    
    //std::cout << "Cycles " << ncycles << std::endl;
    char tempbuffer[2000000];
    int bufferSize = sizeof(tempbuffer);
    std::string tmpString;
    
    bool first = true;
    // high_resolution_clock::time_point t1 = high_resolution_clock::now();  
    while(fDataSocket->checkselect() > 0){
      
       int bytes = fDataSocket->read(tempbuffer, bufferSize); 

      first = false;
      //std::cout << "Got bytes " << bytes << std::endl;
      if(bytes > 0){
	tmpString.append(tempbuffer,bytes);
      }else{
	std::cerr << "CVICPClient::getStringResponse : error no bytes read." << bytes << std::endl;
	exit(0);
      }
    }
      
    //high_resolution_clock::time_point t2 = high_resolution_clock::now();
      
    //duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    //if(first)std::cout << "getStringResponseP " << time_span.count() << "\n";  

     // and "" << time_span2.count() << " seconds." << std::endl;
    // If checkselect didn't find anything, just wait.
    
    if(tmpString.length() == 0){
      usleep(10);
      continue;
    }

    //std::cout << "tmpString " << tmpString.length() << std::endl;
  
    bool stripping_status = stripVICPHeaders(tmpString, stringResponse, lastPacketHasEOI);

    if(!stripping_status){
      std::cout << "Something went wrong, VICP packet seems to have been spread across multiple TCP packets." << std::endl;
      
      // Try to read more data from buffer, keep going for a little while...
      int total_extra_bytes = 0;
      for(int i = 0; i < 10; i++){
	usleep(100000);
	int bytes;
	while(fDataSocket->checkselect() > 0){
	  
	  bytes = fDataSocket->read(tempbuffer, bufferSize); 
	  
	  first = false;
	  if(bytes > 0){
	    total_extra_bytes += bytes;
	  }else{
	    std::cerr << "CVICPClient::getStringResponse : error no bytes read." << bytes << std::endl;
	    exit(0);
	  }
	}
	std::cout << "Now got " << total_extra_bytes << std::endl;
	if(bytes > 8){
	  unsigned int *asint = ((unsigned int *) (tempbuffer + bytes -8));

	  unsigned int packet_length = ntohl(asint[1]);
	  unsigned int sequence_number = ((asint[0] & 0xff0000) >> 16);
	  unsigned int shortstart = asint[0];


	  std::cout << std::hex << ((unsigned int *) (tempbuffer + bytes -4))[0]
		    << " " << shortstart << std::dec << std::endl;


	    }
      }
      std::cout << "Total extra bytes read from socket: " << total_extra_bytes << std::endl;
      return false;

    }
    //std::cout << "Found EOI packet: " << lastPacketHasEOI << std::endl;
    
    ncycles++;
    if(ncycles>100){
      std::cout << "Hmm, did 100 cycles of checking for TCP packets... something wrong." << std::endl;
    }
  }

  return true;


}


