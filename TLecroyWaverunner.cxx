#include "TLecroyWaverunner.h"




#include <arpa/inet.h>


TLecroyWaverunner::TLecroyWaverunner(std::string ip){

  std::cout << "Connect to Lecroy Waverunner at IP " << ip << std::endl;

  fCVICPClient = new CVICPClient();
  bool connStatus = fCVICPClient->connectToDevice(ip);
  if(connStatus){
    std::cout << "Connection to Lecroy Waverunner successful" << std::endl;
  }else{
    std::cerr << "Connection failed." <<std::endl;
  }

  // Send a generic identification request, to ensure that connection works. 
  std::string response = SendRequestGetStringResponse("*IDN?");

  std::cout << "Lecroy scope identified itself as \n" << response << std::endl;
  
}

bool TLecroyWaverunner::SendCommand(std::string commandString){

  bool status = fCVICPClient->sendDataToDevice(commandString,true);
  if(!status){
    std::cout << "SendCommand: failed to send data to device" << std::endl;
  }

  return status;
}
#include <chrono>

std::string TLecroyWaverunner::SendRequestGetStringResponse(std::string requestString){
  
  //std::cout << "!!! Request " << requestString << std::endl;
  //using namespace std::chrono;
  //high_resolution_clock::time_point t1 = high_resolution_clock::now();  
  bool status = fCVICPClient->sendDataToDevice(requestString,true);
  if(!status){
    std::cout << "SendRequestGetStringResponse: failed to send data to device" << std::endl;
  }

  // wait up to 1sec for response.  Some commands seem to take quite a while.
  for(int i = 0; i < 100000; i++){
    usleep(10);
    if(fCVICPClient->fDataSocket->available())
      break;
  }
  // high_resolution_clock::time_point t2 = high_resolution_clock::now();

  std::string response;
  status = fCVICPClient->getStringResponse(response);
  if(!status){
    std::cout << "SendRequestGetStringResponse: failed to receive response from device" << std::endl;
  }

  //     high_resolution_clock::time_point t3 = high_resolution_clock::now();
  // 
  // duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
  // duration<double> time_span2 = duration_cast<duration<double>>(t3 - t2);
  // std::cout << "VICP " << time_span.count() << " and " << time_span2.count() << " seconds." << std::endl;

  return response;
}

std::string TLecroyWaverunner::GetWaveform(std::string requestString){

 
  bool status = fCVICPClient->sendDataToDevice(requestString,true);
  if(false){
    std::cout << "SendRequestGetStringResponse: failed to send data to device" << std::endl;
  }

  // wait up to 1sec for response.  Some commands seem to take quite a while.
  for(int i = 0; i < 100000; i++){
    usleep(10);
    if(fCVICPClient->fDataSocket->available())
      break;
  }

  std::string response;
  //  status = fCVICPClient->getWaveformData(response);
  status = fCVICPClient->getStringResponse(response);
  if(!status){
    std::cout << "SendRequestGetStringResponse: failed to receive correct response from device" << std::endl;
    response = std::string("COM_FAILURE_GET_WAVEFORM");    
  }

  return response;
}


// I hate string manipulation in C/C++
//  tempbuffer[bytes-2] = NULL; // Get rid of last two char = '\n:'
// std::string tmpstr(tempbuffer);
// std::string tmpstr2 = tmpstr.substr(1); // Get rid of first char (blank)
// float value = atof(tmpstr2.c_str());

