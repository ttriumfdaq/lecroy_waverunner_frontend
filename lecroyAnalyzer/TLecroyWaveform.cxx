#include "TLecroyWaveform.h"

#include "TLecroyData.hxx"
#include "TDirectory.h"


/// Reset the histograms for this canvas
TLecroyWaveform::TLecroyWaveform(){

  CreateHistograms();

}


void TLecroyWaveform::CreateHistograms(int nbins, double timeBase ){


  fNBins = nbins;
  fTimeBase = timeBase;

  // check if we already have histogramss.
  char tname[100];
  sprintf(tname,"Lecroy_%i",0);
  
  TH1D *tmp = (TH1D*)gDirectory->Get(tname);
  if (tmp) return;
  
  // Otherwise make histograms
  clear();
  
  for(int i = 0; i < 4; i++){ // loop over 2 channels
    
    char name[100];
    char title[100];
    sprintf(name,"Lecroy_%i",i);
    
    sprintf(title,"Lecroy Waveform for channel=%i",i);	
    
    // Defaults are irrelevant.   We will rebin once we see data packet.
    TH1D *tmp = new TH1D(name, title, 10, 0, 1);
    tmp->SetXTitle("ns");
    tmp->SetYTitle("volt");
    
    push_back(tmp);
  }
  std::cout << "TLecroyWaveform done init...... " << std::endl;
  
}


void TLecroyWaveform::UpdateHistograms(TDataContainer& dataContainer){
  
  
  int eventid = dataContainer.GetMidasData().GetEventId();
  int timestamp = dataContainer.GetMidasData().GetTimeStamp();

  std::cout << "Time " << timestamp << std::endl;
  for(int ch = 0; ch < 4; ch++){

    std::cout << ch << " ch << " << std::endl;
    char name[5];
    sprintf(name,"LCR%i",ch);

    TLecroyData *lecroyTrace = dataContainer.GetEventData<TLecroyData>(name);

  
    if(!lecroyTrace )
      continue;

    std::cout << "Vertical gain "  << lecroyTrace->GetVerticalGain()   << std::endl;
    std::cout << "Vertical offset "  << lecroyTrace->GetVerticalOffset()   << std::endl;
    std::cout << "Horizontal Intervale "  << lecroyTrace->GetHorizontalInterval()   << std::endl;
    
    
    // Check if we need to rebin (for first channel)
    if(ch == 0){
       double timeBase = lecroyTrace->GetHorizontalInterval();
       int nbins = lecroyTrace->GetWaveArraySamples();
       
       if(!(nbins == fNBins && fTimeBase == timeBase) && nbins > 0){
	 
	 std::cout << "Rebin! " << nbins << " " << timeBase << std::endl;
	 fNBins = nbins;
	 fTimeBase = timeBase;
	 for(int i = 0; i < 4; i++)
	   GetHistogram(i)->SetBins(nbins,0,nbins*timeBase*1e9);
       }
    }

    if(lecroyTrace->GetWaveArraySamples() <= GetHistogram(ch)->GetNbinsX())
      for(int bin = 1; bin < lecroyTrace->GetWaveArraySamples() + 1; bin++){
	
	GetHistogram(ch)->SetBinContent(bin,lecroyTrace->GetVoltageValue(bin));
      }

    std::cout << "Finished setting bin contents " << std::endl;

  }
}
