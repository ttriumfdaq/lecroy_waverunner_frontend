#include "TLecroyData.hxx"

#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

class LecroyScopeData {


public:

  LecroyScopeData(std::string response){

  }


};


TLecroyData::TLecroyData(int bklen, int bktype, const char* name, void *pdata):
    TGenericData(bklen, bktype, name, pdata)
{

  //  std::cout << "Block length: " << bklen << std::endl;
  fString = std::string((char*)pdata, bklen*4);

  // WaveDesc part:
  
  bool verbose = true;
  if(verbose){
    //std::cout << "First 16 : " << GetString(0)
    //	<< std::endl;
    //      std::cout << "Second 16 : " << GetString(16) << std::endl;
    
    //std::cout << "COMM_TYPE: " << GetData8()[32] << " " << std::endl;
    
    std::cout << "length WAVEDESC: "      << GetWaveDescLength() << std::endl;
    std::cout << "waveform data length: " << GetWaveArrayLength() << std::endl;
    //std::cout << "waveform data samples: " << GetWaveArraySamples() << std::endl;
    
    
    //std::cout << "Instrument name : " << GetInstrumentName() << std::endl;
    
    
    //std::cout << "Vertical gain "  << GetVerticalGain()   << std::endl;
    //std::cout << "Vertical offset "  << GetVerticalOffset()   << std::endl;
    //std::cout << "Horizontal Intervale "  << GetHorizontalInterval()   << std::endl;
    //std::cout << "Record type "  << GetRecordType()   << std::endl;
    //std::cout << "Coupling "  << GetCoupling() << std::endl;
    //std::cout << "Get comm order "  << GetCommOrder() << std::endl;
    
    std::cout << "Channel: " << GetChannel() << std::endl;
    
  }
  
  // Actual waveform information...
  
  
  if(0)
    for(int i =0; i < 20; i++){
      std::cout << i << " 0x" << std::hex << (int) GetMyData8(GetWaveDescLength())[i] << std::dec << " " << (int)GetMyData8(GetWaveDescLength())[i]
		<< " " << GetVoltageValue(i) << std::endl;
    }
  

}

void TLecroyData::Print(){

  std::cout << "Lecroy decoder for bank " << GetName().c_str() << std::endl;


}
