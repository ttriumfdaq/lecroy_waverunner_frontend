#ifndef TLecroyWaveform_h
#define TLecroyWaveform_h

#include <string>
#include "THistogramArrayBase.h"

/// Class for making histograms of raw Lecroy scope traces
class TLecroyWaveform : public THistogramArrayBase {
public:
  
  TLecroyWaveform();
  virtual ~TLecroyWaveform(){};

  void UpdateHistograms(TDataContainer& dataContainer);

  // Reset the histograms; needed before we re-fill each histo.
  void Reset();

  void CreateHistograms(int nbins=10, double timeBase =1);

  /// Take actions at begin run
  void BeginRun(int transition,int run,int time){		
    CreateHistograms();		
  }

private:
  
  int fNBins;
  double fTimeBase;

};

#endif


