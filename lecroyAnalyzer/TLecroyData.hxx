#ifndef TLecroyData_hxx_seen
#define TLecroyData_hxx_seen

#include <vector>

#include "TGenericData.hxx"

/// Class for each channel measurement
/// For the definition of obscure variables see the CAEN DT724 manual.
class RawChannelMeasurement {

  friend class TLecroyData;

public:
  
	int GetNSamples(){
		return  fSamples.size();
	}
	
	int GetChannel(){ return fChan;}

  /// Get Errors
  uint32_t GetSample(int i){
		if(i >= 0 && i < fSamples.size())
			return fSamples[i];
		return 9999999;
	}

	void AddSamples(std::vector<uint32_t> Samples){
		fSamples = Samples;
	}

private:

	int fChan; // channel number

  /// Constructor; need to pass in header and measurement.
  RawChannelMeasurement(int chan){
		fChan = chan;
	}

	std::vector<uint32_t> fSamples;


};


/// Class to store raw data from CAEN 100MHz DT724 (for raw readout, no-DPP).
class TLecroyData: public TGenericData {

public:

  /// Constructor
  TLecroyData(int bklen, int bktype, const char* name, void *pdata);


  // length of wave description block, in bytes
  int GetWaveDescLength(){ return GetMyData32(36)[0];};

  // length of waveform array (in bytes)
  int GetWaveArrayLength(){ return GetMyData32(60)[0];};

  // length of waveform samples.
  int GetWaveArraySamples(){ return GetMyData32(116)[0];};

  std::string GetInstrumentName(){ return GetString(76);};

  float GetVerticalGain(){  return GetFloat(156);  };

  float GetVerticalOffset(){  return GetFloat(160);  };

  // Get the sampling interval in time;
  float GetHorizontalInterval(){ return GetFloat(176);  };
  
  int GetRecordType(){  return GetMyData32(316)[0];  };

  int GetCoupling(){return GetMyData32(326)[0];};

  short GetCommOrder(){return GetMyData16(34)[0];};

  short GetChannel(){return GetMyData16(344)[0];};

  // Get voltage value for sample i
  float GetVoltageValue(int i){

    if(i < 0 || i > 11111111) return -9999.0;

    int value = GetMyData8(GetWaveDescLength())[i];
    if(value >= 0x80) value = -256 + value ;

    int value2 = -99999;
    if(GetMyData8(GetWaveDescLength())[i] < 0x80)
      value2 = GetMyData8(GetWaveDescLength())[i];
    else
      value2 = - (int)((GetMyData8(GetWaveDescLength())[i] + 0x80) & 0xff);

    //std::cout << "value " << (int)GetMyData8(GetWaveDescLength())[i] << " " << value << " " << value2 << std::endl;
    return (float)value *GetVerticalGain() - GetVerticalOffset();
  }

  void Print();

  
private:

  std::string  fString;
  int fStringLength;
  
  const uint8_t*  GetMyData8(int byteoffset=0) const { return reinterpret_cast<const uint8_t*>(fString.c_str() + byteoffset); }
  const uint16_t* GetMyData16(int byteoffset=0) const { return reinterpret_cast<const uint16_t*>(fString.c_str() + byteoffset); }

  const uint32_t* GetMyData32(int byteoffset=0) const { return reinterpret_cast<const uint32_t*>(fString.c_str() + byteoffset); }

  float GetFloat(int byteoffset=0) const { return *reinterpret_cast<const float*>(fString.c_str() + byteoffset); }

  std::string GetString(int byteoffset=0) const { return fString.substr(byteoffset,16);}



};

#endif
