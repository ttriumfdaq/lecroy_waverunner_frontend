/********************************************************************\

Frontend for reading Lecroy Waverunner scope.

T. Lindner (Fall 2015)


 $Id:$

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "midas.h"
#include "msystem.h"
#include "mcstd.h"

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/
/* The frontend name (client name) as seen by other MIDAS clients   */
   char *frontend_name = "lecroy_scope";

/* The frontend file name, don't change it */
   char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
   BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
   INT display_period = 0000;

/* maximum event size produced by this frontend */
   INT max_event_size = 1000000;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
   INT max_event_size_frag = 5 * 1024 * 1024;

/* buffer size to hold events */
   INT event_buffer_size = 10 * 1000000;


/* MY HV structure */
  HNDLE hKey;
  extern HNDLE hDB;   //!< main ODB handle                                                                                                          

/*-- Function declarations -----------------------------------------*/

   INT frontend_init();
   INT frontend_exit();
   INT begin_of_run(INT run_number, char *error);
   INT end_of_run(INT run_number, char *error);
   INT pause_run(INT run_number, char *error);
   INT resume_run(INT run_number, char *error);
   INT frontend_loop();
   INT read_scope_event(char *pevent, INT off);
/*-- Equipment list ------------------------------------------------*/

#undef USE_INT

   EQUIPMENT equipment[] = {
 {
   "LECROYSCOPE",                     /* equipment name */
     {
        1, 0,     /* event ID, trigger mask */
       "SYSTEM",                /* write events to system buffer */
	EQ_POLLED,               /* equipment type */
	//EQ_PERIODIC,              /* equipment type */
       LAM_SOURCE(0, 0x0),      /* event source crate 0, all stations */
       "MIDAS",                 /* format */
       TRUE,                    /* enabled */
       RO_RUNNING,              /* read only when running */
       500,                     /* poll for 500ms */
       0,                       /* stop run after this event limit */
       0,                       /* number of sub events */
       0,                       /* don't log history */
       "", "", ""
     },
   read_scope_event,       /* readout routine */
 },


      {""}
   };

#ifdef __cplusplus
}
#endif
/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.
  
  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send 
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.

                                                                                                                                                                  \********************************************************************/

// Pointer to the lecroy waverunner object.
#include "TLecroyWaverunner.h"
TLecroyWaverunner *wave = 0;

int fChannelEnabled[4];

INT initialize_settings(){

  // Grab the current overall settings                                                                                                            
  HNDLE hsf;
  char set_str[200];
  int size;
  sprintf(set_str, "/Equipment/LECROYSCOPE/Settings");
  db_find_key(hDB, 0, set_str, &hsf);

  // Can't do a single call for bool variable... why?  Need to understand this...
  // why doesn't this work
  // bool fChannelEnabled[4];
  // db_get_value(hDB, hsf, "channelEnable",&fChannelEnabled, &size, TID_BOOL, TRUE);
  //for(int i = 0; i < 4; i++){
    //bool test;
    size = sizeof(fChannelEnabled);
   // char name[100];
    //s//printf(name,"channelEnable[%i]",i);
    
    db_get_value(hDB, hsf, "channelEnable",&fChannelEnabled, &size, TID_INT, TRUE);
    //fChannelEnabled[i] = test;
 //` }
  std::cout << "ODB Channel enable list : " << fChannelEnabled[0] << " " 
	    << fChannelEnabled[1] << " " 
	    << fChannelEnabled[2] << " " 
	    << fChannelEnabled[3] << std::endl;

  
  for(int i = 0; i < 4; i++){

    if(!fChannelEnabled[i]){
      cm_msg(MINFO, "initialize_settings", "Channel %i is disabled by ODB.",i);
      continue;
    }

    char command[7];
    sprintf(command,"C%i:TRA?",i+1);
    std::string response = wave->SendRequestGetStringResponse(command);    
    if(response.find("TRA OFF") != std::string::npos){
      cm_msg(MINFO, "initialize_settings", "Channel %i is disabled by Lecroy scope.",i);
      fChannelEnabled[i] = 0;
      continue;
    }

    cm_msg(MINFO, "initialize_settings", "Channel %i is enabled.",i);
  }
  return 0;

}


/*-- Frontend Init -------------------------------------------------*/
    INT frontend_init()
{

  // Initialize object, which establishes connection to Lecroy Waverunner scope
  wave = new TLecroyWaverunner("142.90.103.48");
  
  // Send a generic identification request, to ensure that connection works. 
  std::string response = wave->SendRequestGetStringResponse("TIME_DIV?");
  std::string response2 = wave->SendRequestGetStringResponse("C2:INSPECT? \"WAVEDESC\"");

  //std::cout << "waveform description: "  << response2 << std::endl;
 
  std::string response3 = wave->SendRequestGetStringResponse("template?");

  initialize_settings();
  
  std::cout << "Done initialize" << std::endl;
 
  // Clear current status registers
  wave->SendCommand("*CLS");
  // set scope to allow next trigger (in case a run is already in progress)
  wave->SendCommand("TRMD SINGLE");

   return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
   return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{

  cm_msg(MINFO, "initialize_settings", "Lecroy Scope: Begin-of-run.");

  initialize_settings();

  // Clear current status registers
  wave->SendCommand("*CLS");

  // set scope to allow next trigger
  wave->SendCommand("TRMD SINGLE");
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/

INT end_of_run(INT run_number, char *error)
{
  // Clear current status registers
  wave->SendCommand("*CLS");
  
   return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/

INT pause_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Resuem Run ----------------------------------------------------*/

INT resume_run(INT run_number, char *error)
{
   return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
   return SUCCESS;
}

/*------------------------------------------------------------------*/

/********************************************************************\
  
  Readout routines for different events

\********************************************************************/
#include <chrono>

/*-- Trigger event routines ----------------------------------------*/
extern "C" INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
   int i;
   DWORD lam=0;

   for (i = 0; i < count; i++) {
     
     // Use INR register to check if a trigger has occurred
     std::string inr = wave->SendRequestGetStringResponse("INR?");
     int register_value = strtol(inr.substr(4,4).c_str(),NULL,0);
     //     if(inr.find("INR 0") == std::string::npos)
     //std::cout << "Register " << inr << std::endl;
     //if(register_value & 0x2000)
     if(register_value & 0x1)
       lam = 1;
     if (lam)
       if (!test){
	 //std::cout << "poll " <<inr << " " << register_value << std::endl;
	 return lam;
       }      
     usleep(10);
   }
   return 0;
}

/*-- Interrupt configuration ---------------------------------------*/

extern "C" INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
      break;
   case CMD_INTERRUPT_DISABLE:
      break;
   case CMD_INTERRUPT_ATTACH:
      break;
   case CMD_INTERRUPT_DETACH:
      break;
   }
   return SUCCESS;
}

void fillBankForChannel(int ch, char *pevent){

  
  uint32_t *pdata32;
  char bname[5];
  sprintf(bname,"LCR%i",ch);

    
  // Get waveform
  char command[50];
  sprintf(command,"C%i:WAVEFORM? ALL",ch+1);
  std::string response = wave->GetWaveform(command);
  if(response.find("COM_FAILURE_GET_WAVEFORM") != std::string::npos){
    std::cout << "Not filling bank " << bname << std::endl;
    //bk_close(pevent, pdata32);    
    return;
  }

  // We got data, so create bank.
  bk_create(pevent, bname, TID_DWORD, (void **)&pdata32);

  // Check the total length... better be correct.
  int reported_length = atoi(response.substr(12,9).c_str());
						
  // Strip off the first 21 bytes off data packet; they just contain the length of main data.
  std::string stripped = response.substr(21);
  if(stripped.size() > 0) stripped.resize(stripped.size()-1);

  // Sometimes there is a EOL character added to the end... not sure why...
  if(reported_length+1 != stripped.length() && reported_length != stripped.length()){
    std::cerr << "LecroyScopeData:: The reported length of data packet (" << reported_length +1
	      << ") does not match actual length (" <<stripped.length()  << std::endl;
    exit(0);
  }
  if(reported_length %4 == 0) {
    int total_32words = reported_length / 4;
    unsigned int *array = (unsigned int *) stripped.c_str();
    for(int i = 0; i < total_32words; i++)
      *pdata32++ = array[i];
  }else{
    if(0)std::cout << "Data bank not aligned on 32bit (size = " << reported_length 
	      << ")... not going to work... channel " << ch 
	      << std::endl;
  }




  int size2 = bk_close(pevent, pdata32);    

}

double total_time = 0.0;
double total_checks = 0.0;

/*-- Tiny event --------------------------------------------------*/
#define NWORDS 3
INT read_scope_event(char *pevent, INT offset)
{

  static WORD *pdata = NULL;
  static WORD sub_counter = 0;  /* init bank structure */
  bk_init32(pevent);

  static int event_counter = 0;
  event_counter++;
  if(event_counter%20==0)std::cout << ".";
 
   using namespace std::chrono;
  high_resolution_clock::time_point t1 = high_resolution_clock::now();  
  for(int i = 0; i < 4; i++)
    if(fChannelEnabled[i]) fillBankForChannel(i,pevent);

  high_resolution_clock::time_point t2 = high_resolution_clock::now();

  duration<double> time_span = duration_cast<duration<double>>(t2 - t1);

  if(event_counter%100==0){
    total_time += time_span.count() ;
    total_checks += 1.0;

  std::cout << "It took me " << time_span.count() << " seconds.";
  std::cout << " average " << total_time/total_checks
	    << " total events " << total_checks ;
  std::cout << std::endl;
  }

  //std::string inr = wave->SendRequestGetStringResponse("INR?");
  // std::cout << "inr " << inr <<std::endl;
  // Clear current status registers
  //wave->SendCommand("*CLS");

  // set scope to allow next trigger
  wave->SendCommand("TRMD SINGLE");

  //    std::string inr2 = wave->SendRequestGetStringResponse("INR?");
     //std::cout << "inr2 " << inr <<std::endl;

  return bk_size(pevent);

}
