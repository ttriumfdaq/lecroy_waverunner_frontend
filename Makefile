# Makefile
#
# $Id$
#

OSFLAGS  = -DOS_LINUX -Dextname
CFLAGS   = -g -O2 -fPIC -Wall -Wuninitialized -I. -I$(MIDASSYS)/include -std=c++0x
CXXFLAGS = $(CFLAGS)

LIBS = -lm -lz -lutil -lnsl -lpthread -lrt
LIB_DIR = $(MIDASSYS)/linux/lib

# MIDAS library
MIDASLIBS = $(MIDASSYS)/linux/lib/libmidas.a

# ROOT library
ifdef ROOTSYS
CXXFLAGS += -DHAVE_ROOT -I$(ROOTSYS)/include -I$(ROOTSYS)/include/root
#ROOTGLIBS = $(shell $(ROOTSYS)/bin/root-config --glibs) -lThread -Wl,-rpath,$(ROOTSYS)/lib/root
ROOTGLIBS = $(shell $(ROOTSYS)/bin/root-config --glibs)  -Wl,-rpath,$(shell $(ROOTSYS)/bin/root-config --libdir)
LIBS += $(ROOTGLIBS)
endif

all:: test.exe lecroyfe.exe



lecroyfe.exe: %.exe:   %.o KOsocket.o VICPClient.o TLecroyWaverunner.o
	$(CXX) -o $@ $(CFLAGS) $(OSFLAGS) $^ $(MIDASLIBS) $(LIB_DIR)/mfe.o $(MIDASLIBS) $(LIBS)

test.exe: %.exe:   %.o  KOsocket.o VICPClient.o TLecroyWaverunner.o
	$(CXX) -o $@ $(CFLAGS) $(OSFLAGS) $^ $(LIBS)


%.o: %.cxx
	$(CXX) $(CXXFLAGS) $(OSFLAGS) -c $<

%.o: %.c
	$(CXX) $(CXXFLAGS) $(OSFLAGS) -c $<

clean::
	-rm -f *.o *.exe

# end
